# ! /usr/bin/python

from neuralimg.training import siamese as si
from neuralimg.training import net_utils as nu


dataset = '/DataDisk/morad/out/triplet.h5'
model = 'model'
logs = 'logs'

siamese = si.TripletSiamese(dataset)
siamese.test(model, logs, nu.l2, 1000)

# siamese = TripletSiamese(dataset)
# losses = siamese.get_test_loss(model, logs)
# print('Mean loss: %f' % np.mean(losses))

