# !/usr/bin/python

from neuralimg.image import preproc

""" Preprocesses groundtruth so it joins small supervoxels with neighboring ones
and gives separate ids to unconnected regions even if they belong to the same neuron
(for ease of 2D evaluation) """

# From folder
p = preproc.DatasetProc('/DataDisk/morad/snemi/groundtruth/')
p.read()
p.join_small()
p.save_data('/DataDisk/morad/snemi/groundtruth_large/')
p.split_labels()
p.save_data('/DataDisk/morad/snemi/groundtruth_large_unconnected/')

