# !/usr/bin/python

from neuralimg.image import preproc as pr

""" Generates superpixels given the membrane probabilities """

proc = pr.DatasetProc('data/crag/mem')
proc.read()
proc.segment(7, 0.25)
proc.save_data('data/crag/sps')
