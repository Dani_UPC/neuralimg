This repository contains utilities to deal with a neural reconstruction problem.

Instructions to install:

- Beforehand, the following utilities must be installed:
  - Candidate_mc: https://github.com/DaniUPC/candidate_mc
  - TED: https://github.com/DaniUPC/ted

Afterwards we need our PATH variable to point at the build/binaries folder within each of the previous folders. 
Assuming both folder:

export PATH=$PATH:$HOME/candidate_mc/build/binaries:$HOME/ted/build/binaries/

And PYTHONPATH variable must point to the build/python folder. Assuming, again, that both folders are in the home directory:

export PYTHONPATH=$PYTHONPATH:$HOME/candidate_mc/build/python

Finally, to install this package:

git clone XXXX

And you can load it using the PYTHONPATH variable pointing to the root folder of the project:

export PYTHONPATH=$PYTHONPATH:$HOME/code # TODO

Usage

In order to use it we must import the python environment using Conda (http://conda.pydata.org/miniconda.html)

conda env create -f iri.yml

Then, we must activate it by:

source activate iri


Use cases

- Evaluation of superpixel segmentations: tests/evaluate_grid.py. Results can be explored in notebooks/grid_analysis.ipynb
- Given segmentations, we can evaluate up to which threshold we must merge adjacent candidates
  in order to properly approximate the groundtruth: tests/evaluate_merges.py
- In order to create CRAGS, we can use tests/crag_gen_example.py
- To extract datasets from a CRAG solution we can use tests/create_dataset.py. Then, resulting data can be visualized using notebooks/dataset_visualization.ipynb

Then, we can execute any of the tests in the package.

Gala tree extraction

Download Gala from this link https://github.com/DaniUPC/gala and import the environment gala from the environment folder. Then, call:

python setup.py install 

Within the gala directory
