#!/usr/bin/python

from neuralimg.evaluation import speval as ev
import os

# Important: for a fair evaluation, segments from neurons that are the same
# but not connected should be separated in an updated groundtruth version (what
# we call 'unconnected groundtruth')

superpixels = '/DataDisk/morad/snemi/superpixels/watershed/mask7_sigma025'
truth = '/DataDisk/morad/snemi/groundtruth_unconnected'
ted_shift = 25
split = True

# Sample A - Candidate MC
histories = os.path.join(superpixels, 'histories_mc')
print('Evaluating candidate MC merge trees ...')
best, data = ev.search_threshold(superpixels, truth, histories, ted_shift,
    out_stats=os.path.join(superpixels, 'mc_merges.dat'), split_bg=split)
print('Best configuration found:')
print(best)

# Sample A - Gala
histories = os.path.join(superpixels, 'histories_gala')
print('Evaluating gala merge trees ...')
best, data = ev.search_threshold(superpixels, truth, histories, ted_shift,
    out_stats=os.path.join(superpixels, 'gala_merges.dat'), split_bg=split)
print('Best configuration found:')
print(best)
