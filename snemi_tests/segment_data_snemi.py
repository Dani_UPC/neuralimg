# !/usr/bin/python

from neuralimg.image import preproc as pr

# Snemi C
proc = pr.DatasetProc('/DataDisk/morad/snemi/membrane/')
proc.read()
proc.segment(7, 0.25)
proc.save_data('/DataDisk/morad/snemi/superpixels/watershed/mask7_sigma025_ted25')
