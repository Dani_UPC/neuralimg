#!/usr/bin/python

from neuralimg.crag import crag as cr
import os

if __name__ == '__main__':

    gt = '/DataDisk/morad/snemi/groundtruth/'
    sps_mc = '/DataDisk/morad/snemi/superpixels/watershed/mask7_sigma025_ted25_thresh_mc007749/'
    sps_gala = '/DataDisk/morad/snemi/superpixels/watershed/mask7_sigma025_ted25_thresh_gala04/'
    raws = '/DataDisk/morad/snemi/raw/'
    histories_mc = '/DataDisk/morad/snemi/superpixels/watershed/mask7_sigma05_ted25/histories_mc'
    histories_gala = '/DataDisk/morad/snemi/superpixels/watershed/mask7_sigma05_ted25/histories_gala'
    membranes = '/DataDisk/morad/snemi/membrane_norm/'
    out_gala = '/DataDisk/morad/snemi/crags/gala_heuristic'
    out_mc = '/DataDisk/morad/snemi/crags/mc_heuristic'

    create_conf = '/DataDisk/morad/snemi/crags/create_training_project.conf'
    features_conf = '/DataDisk/morad/snemi/crags/extract_training_features.conf'
    effort_conf = '/DataDisk/morad/snemi/crags/extract_best-effort.conf'

    d = 450

    # CRAG FOR MC SUPERPIXELS
    cragen = cr.CragGenerator(out_mc)
    cragen.generate_crag(gt, sps_mc, raws, membranes, create_conf,
        max_zlink=d, histories=histories_mc, histories_thresh=0.07749)
    cragen.extract_features(features_conf)
    cragen.extract_best_effort(effort_conf, cr.LossType.HEURISTIC, os.path.join(out_mc, 'best'))

    # CRAG FOR GALA SUPERPIXELS
    cragen = cr.CragGenerator(out_gala)
    cragen.generate_crag(gt, sps_gala, raws, membranes, create_conf,
        max_zlink=d, histories=histories_gala, histories_thresh=0.4)
    cragen.extract_features(features_conf)
    cragen.extract_best_effort(effort_conf, cr.LossType.HEURISTIC, os.path.join(out_gala, 'best'))

