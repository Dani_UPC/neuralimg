#!/usr/bin/python

from neuralimg.crag import merge_trees as mt
import os

if __name__ == '__main__':

    # Debug: SNEMI
    gt_folder = '/DataDisk/morad/snemi/groundtruth_unconnected'
    sps_folder = '/DataDisk/morad/snemi/superpixels/watershed/mask7_sigma025'
    mems_folder = '/DataDisk/morad/snemi/membrane_norm'

    # MC Extractor, test both formats
    mc_path = os.path.join(sps_folder, 'histories_mc')
    mt.MCTreeExtractor(sps_folder, mems_folder).extract(mc_path)

    # Using gala with some subset data
    g_path = os.path.join(sps_folder, 'histories_gala')
    mt.GalaTreeExtractor(sps_folder, mems_folder, gt_folder).extract(g_path)

