#!/usr/bin/python

from neuralimg.evaluation import speval as ev

gts = '/DataDisk/morad/snemi/groundtruth/'
sps_mc = '/DataDisk/morad/snemi/superpixels/watershed/mask7_sigma025_ted25_thresh_mc007749/'
sps_gala = '/DataDisk/morad/snemi/superpixels/watershed/mask7_sigma025_ted25_thresh_gala04/'
raws = '/DataDisk/morad/snemi/raw/'
histories_gala = '/DataDisk/morad/snemi/superpixels/watershed/mask7_sigma025_ted25/histories_gala'
histories_mc = '/DataDisk/morad/snemi/superpixels/watershed/mask7_sigma025_ted25/histories_mc'
membranes = '/DataDisk/morad/snemi/membrane_norm'
tmp='/DataDisk/morad/test/snemi'

create_conf = '/DataDisk/morad/snemi/crags/create_training_project.conf'
features_conf = '/DataDisk/morad/snemi/crags/extract_training_features.conf'
effort_conf = '/DataDisk/morad/snemi/crags/extract_best-effort.conf'

out_mc = '/DataDisk/morad/snemi/crags/stats_mc2.data'
out_gala = '/DataDisk/morad/snemi/crags/stats_gala2.data'
dists = [450, 550, 650]

# MC evaluation
ev.evaluate_crags(sps_mc, gts, raws, membranes, histories_mc, create_conf, features_conf, 
    effort_conf, dists, nworkers=3, thresh=0.07749, outp=out_mc, tmp=tmp)

# Gala evaluation
ev.evaluate_crags(sps_gala, gts, raws, membranes, histories_gala, create_conf, features_conf, 
    effort_conf, dists, nworkers=3, thresh=0.40, outp=out_gala, tmp=tmp)
