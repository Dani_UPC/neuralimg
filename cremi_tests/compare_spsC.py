# !/usr/bin/python

from neuralimg.image import preproc as pr

# Sample A - merge small superpixels to avoid so many split errors
proc = pr.DatasetProc('/DataDisk/morad/cremi/superpixels/sampleC/mask11_sigma025_ted25')
proc.read()
proc.join_small(min_region_area=20)
proc.save_data('/DataDisk/morad/cremi/superpixels/sampleC/mask11_sigma025_ted25_large')

from neuralimg.evaluation import speval as ev

sp_src = '/DataDisk/morad/cremi/superpixels/sampleC/mask11_sigma025_ted25'
gt = '/DataDisk/morad/cremi/groundtruth_large_unconnected/sampleA'
sp_large = '/DataDisk/morad/cremi/superpixels/sampleC/mask11_sigma025_ted25_large'

# Valuate base case
rand, voi = ev.evaluate_supervoxels(sp_src, gt)
print('Sample A source obtained RAND: %f, VOI split: %f, Voi merge: %f' %
    (rand, voi[0], voi[1]))

# Valuate base case
rand, voi = ev.evaluate_supervoxels(sp_large, gt)
print('Sample A source obtained RAND: %f, VOI split: %f, Voi merge: %f' %
    (rand, voi[0], voi[1]))
