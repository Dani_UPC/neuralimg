# !/usr/bin/python

from neuralimg.image import datasets as dd

data_conf = '/DataDisk/morad/cremi/datasets/data.conf'
crag_path = '/DataDisk/morad/cremi/crags/sampleC/mc_heuristic/hdf/training_dataset.h5'
data_path = '/DataDisk/morad/cremi/datasets/dataC.h5'

print('Testing triplet dataset ...')
dgen_pair = dd.TripletDataGen(data_conf, data_path)
cragC = dd.DataGenOptions(crag_path, init=0, end=100, exclude=[14,74,86])
dgen_pair.generate_dataset([cragC])
