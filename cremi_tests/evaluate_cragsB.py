#!/usr/bin/python

from neuralimg.evaluation import speval as ev

gts = '/DataDisk/morad/cremi/groundtruth_large/sampleB'
sps_mc = '/DataDisk/morad/cremi/superpixels/sampleB/mask11_sigma05_ted25_mc00938/'
sps_gala = '/DataDisk/morad/cremi/superpixels/sampleB/mask11_sigma05_ted25_gala052/'
raws = '/DataDisk/morad/cremi/raw/sampleB'
histories_gala = '/DataDisk/morad/cremi/superpixels/sampleB/mask11_sigma05_ted25/histories_gala'
histories_mc = '/DataDisk/morad/cremi/superpixels/sampleB/mask11_sigma05_ted25/histories_mc'
membranes = '/DataDisk/morad/cremi/membrane_norm/sampleB'
tmp='/DataDisk/morad/test/sampleB'

create_conf = '/DataDisk/morad/cremi/crags/create_training_project.conf'
features_conf = '/DataDisk/morad/cremi/crags/extract_training_features.conf'
effort_conf = '/DataDisk/morad/cremi/crags/extract_best-effort.conf'

dists = [650]

# MC
ev.evaluate_crags(sps_mc, gts, raws, membranes, histories_mc, create_conf, features_conf,
    effort_conf, dists, nworkers=3, thresh=0.0938, outp='stats_cragB_mc.dat', tmp=tmp)

# GALA
ev.evaluate_crags(sps_gala, gts, raws, membranes, histories_gala, create_conf, features_conf,
    effort_conf, dists, nworkers=3, thresh=0.52, outp='stats_cragB_gala.dat', tmp=tmp)
