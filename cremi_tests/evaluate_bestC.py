#!/usr/bin/python

from neuralimg.evaluation import speval as ev

# Important: for a fair evaluation, segments from neurons that are the same
# but not connected should be separated in an updated groundtruth version (what
# we call 'unconnected groundtruth')

superpixels_gala = '/DataDisk/morad/cremi/crags/sampleC/gala_heuristic/best'
superpixels_mc = '/DataDisk/morad/cremi/crags/sampleC/mc_heuristic/best'
truth = '/DataDisk/morad/cremi/groundtruth_large/sampleC'

# GALA
rand_gala, voi_gala = ev.evaluate_volumes(superpixels_gala, truth)

# MC
rand_mc, voi_mc = ev.evaluate_volumes(superpixels_mc, truth)

print('Gala scores. RAND: %f, VOI: %f' % (rand_gala, voi_gala))
print('MC scores. RAND: %f, VOI: %f' % (rand_mc, voi_mc))
