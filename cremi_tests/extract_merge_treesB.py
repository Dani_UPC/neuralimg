#!/usr/bin/python

from neuralimg.crag import merge_trees as mt
import os

if __name__ == '__main__':

    # Debug: CREMI
    gt_folder = '/DataDisk/morad/cremi/groundtruth_large_unconnected/sampleB'
    sps_folder = '/DataDisk/morad/cremi/superpixels/sampleB/mask11_sigma05_ted25'
    mems_folder = '/DataDisk/morad/cremi/membrane_norm/sampleB'

    # MC Extractor, test both formats
    mc_path = os.path.join(sps_folder, 'histories_mc')
    mt.MCTreeExtractor(sps_folder, mems_folder).extract(mc_path)

    # Using gala with some subset data
    g_path = os.path.join(sps_folder, 'histories_gala')
    mt.GalaTreeExtractor(sps_folder, mems_folder, gt_folder).extract(g_path)

