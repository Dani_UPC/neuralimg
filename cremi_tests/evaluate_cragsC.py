#!/usr/bin/python

from neuralimg.evaluation import speval as ev

gts = '/DataDisk/morad/cremi/groundtruth_large/sampleC'
sps_mc = '/DataDisk/morad/cremi/superpixels/sampleC/mask11_sigma025_ted25_mc01008627/'
sps_gala = '/DataDisk/morad/cremi/superpixels/sampleC/mask11_sigma025_ted25_gala064019/'
raws = '/DataDisk/morad/cremi/raw/sampleC'
histories_gala = '/DataDisk/morad/cremi/superpixels/sampleC/mask11_sigma025_ted25/histories_gala'
histories_mc = '/DataDisk/morad/cremi/superpixels/sampleC/mask11_sigma025_ted25/histories_mc'
membranes = '/DataDisk/morad/cremi/membrane_norm/sampleC'
tmp='/DataDisk/morad/test/sampleC'

create_conf = '/DataDisk/morad/cremi/crags/create_training_project.conf'
features_conf = '/DataDisk/morad/cremi/crags/extract_training_features.conf'
effort_conf = '/DataDisk/morad/cremi/crags/extract_best-effort.conf'

dists = [650]

# MC
ev.evaluate_crags(sps_mc, gts, raws, membranes, histories_mc, create_conf, features_conf,
    effort_conf, dists, nworkers=3, thresh=0.1008627, outp='stats_cragC_mc.dat', tmp=tmp)

# GALA
ev.evaluate_crags(sps_gala, gts, raws, membranes, histories_gala, create_conf, features_conf,
    effort_conf, dists, nworkers=3, thresh=0.64019, outp='stats_cragC_gala.dat', tmp=tmp)
