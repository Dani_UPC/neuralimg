# !/usr/bin/python

from neuralimg.image import preproc as pr

# Cremi - Sample B
proc = pr.DatasetProc('/DataDisk/morad/cremi/membrane/sampleB')
proc.read()
proc.segment(11, 0.5)
proc.save_data('/DataDisk/morad/cremi/superpixels/sampleB/mask11_sigma05_ted25')
