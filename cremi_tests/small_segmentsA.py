# !/usr/bin/python

from neuralimg.image import preproc as pr

# Sample A - merge small superpixels to avoid so many split errors
proc = pr.DatasetProc('/DataDisk/morad/cremi/superpixels/sampleA/mask3_sigma1_ted25')
proc.read()
proc.join_small(min_region_area=15)
proc.save_data('/DataDisk/morad/cremi/superpixels/sampleA/mask3_sigma1_ted25_large')

