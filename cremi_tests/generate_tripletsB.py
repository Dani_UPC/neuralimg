# !/usr/bin/python

from neuralimg.image import datasets as dd

data_conf = '/DataDisk/morad/cremi/datasets/data.conf'
crag_path = '/DataDisk/morad/cremi/crags/sampleB/mc_heuristic_small22328/hdf/training_dataset.h5'
data_path = '/DataDisk/morad/cremi/datasets/dataB_small.h5'

print('Testing triplet dataset ...')
dgen_pair = dd.TripletDataGen(data_conf, data_path)
cragB = dd.DataGenOptions(crag_path, init=0, end=100, exclude=[15,16,44,45,77])
dgen_pair.generate_dataset([cragB])
