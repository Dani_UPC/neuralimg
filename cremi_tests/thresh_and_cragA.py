#!/usr/bin/python

from neuralimg.evaluation import speval as ev
from neuralimg.crag import crag as cr
from neuralimg.crag import merge_trees as mt
import os
import tempfile

""" Searches for the proper threshold in the merge history and creates the corresponding CRAG """

if __name__ == '__main__':

    root = '/DataDisk/morad/cremi/superpixels/sampleA/'

    superpixels = os.path.join(root, 'mask3_sigma1_ted25')
    truth = '/DataDisk/morad/cremi/groundtruth_large_unconnected/sampleA'
    histories = os.path.join(superpixels, 'histories_mc_small15')
    ted_shift = 25
    split_bg = False

    # Search best threshold in the merge tree so splits are around 10 * merges
    best, _ = ev.search_threshold(superpixels, truth, histories, ted_shift, 
        stop=True, workers=3)
    thresh = best['weighted']
    print('Best threshold is {}'.format(thresh))

    # Generate CRAG given the threshold

    # Generate superpixels in temporary folder
    tmp = tempfile.mkdtemp()
    mt.merge_dataset(superpixels, histories, thresh, tmp)

    str_thresh = str(thresh).replace('.', '')
    membranes = '/DataDisk/morad/cremi/membrane_norm/sampleA'
    raws = '/DataDisk/morad/cremi/raw/sampleA'
    out_mc = '/DataDisk/morad/cremi/crags/sampleA/mc_heuristic_small' + str_thresh

    # Static configuration
    create_conf = '/DataDisk/morad/cremi/crags/create_training_project.conf'
    features_conf = '/DataDisk/morad/cremi/crags/extract_training_features.conf'
    effort_conf = '/DataDisk/morad/cremi/crags/extract_best-effort.conf'

    # CRAG FOR MC SUPERPIXELS
    cragen = cr.CragGenerator(out_mc)
    cragen.generate_crag(truth, tmp, raws, membranes, create_conf,
        max_zlink=450, histories=histories, histories_thresh=thresh)
    cragen.extract_features(features_conf)
    cragen.extract_best_effort(effort_conf, cr.LossType.HEURISTIC, os.path.join(out_mc, 'best'))

