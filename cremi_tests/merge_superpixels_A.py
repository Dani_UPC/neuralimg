#!/usr/bin/python

from neuralimg.crag import merge_trees as mt
import os

if __name__ == '__main__':

    root = '/DataDisk/morad/cremi/superpixels/sampleA'
    sps = os.path.join(root, 'mask3_sigma1_ted25')

    # Gala superpixels
    gala_thresh = 0.6835
    histories_gala = os.path.join(sps, 'histories_gala')
    out_gala = os.path.join(root, 'sigma1_ted25_thresh_gala' + str(gala_thresh).replace('.', ''))
    mt.merge_dataset(sps, histories_gala, gala_thresh, out_gala)

    # MC superpixels
    gala_thresh = 0.060392
    histories_gala = os.path.join(sps, 'histories_mc')
    out_gala = os.path.join(root, 'sigma1_ted25_thresh_mc' + str(gala_thresh).replace('.', ''))
    mt.merge_dataset(sps, histories_gala, gala_thresh, out_gala)

