# !/usr/bin/python

from neuralimg.evaluation import speval as ev

sp_src = '/DataDisk/morad/cremi/superpixels/sampleA/mask3_sigma1_ted25'
gt = '/DataDisk/morad/cremi/groundtruth_large_unconnected/sampleA'
sp_large ='/DataDisk/morad/cremi/superpixels/sampleA/mask3_sigma1_ted25_large'

# Valuate base case
rand, voi = ev.evaluate_supervoxels(sp_src, gt)
print('Sample A source obtained RAND: %f, VOI split: %f, Voi merge: %f' %
    (rand, voi[0], voi[1]))

# Valuate base case
rand, voi = ev.evaluate_supervoxels(sp_large, gt)
print('Sample A source obtained RAND: %f, VOI split: %f, Voi merge: %f' %
    (rand, voi[0], voi[1]))

