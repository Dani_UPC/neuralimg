#!/usr/bin/python

from neuralimg.evaluation import speval as ev

gts = '/DataDisk/morad/cremi/groundtruth_large/sampleA'
sps_mc = '/DataDisk/morad/cremi/superpixels/sampleA/mask3_sigma1_ted25_thresh_mc0060392/'
sps_gala = '/DataDisk/morad/cremi/superpixels/sampleA/mask3_sigma1_ted25_thresh_gala06835/'
raws = '/DataDisk/morad/cremi/raw/sampleA'
histories_gala = '/DataDisk/morad/cremi/superpixels/sampleA/mask3_sigma1_ted25/histories_gala'
histories_mc = '/DataDisk/morad/cremi/superpixels/sampleA/mask3_sigma1_ted25/histories_mc'
membranes = '/DataDisk/morad/cremi/membrane_norm/sampleA'
tmp='/DataDisk/morad/test/sampleA'

create_conf = '/DataDisk/morad/cremi/crags/create_training_project.conf'
features_conf = '/DataDisk/morad/cremi/crags/extract_training_features.conf'
effort_conf = '/DataDisk/morad/cremi/crags/extract_best-effort.conf'

dist = 550

# MC evaluation
stats_mc = ev.evaluate_crag(sps_mc, gts, raws, membranes, histories_mc, create_conf, features_conf,
    effort_conf, dist, thresh=0.060392, tmp=tmp)

print('Candidate mc:')
print(stats_mc)

# Gala evaluation
stats_gala = ev.evaluate_crag(sps_gala, gts, raws, membranes, histories_gala, create_conf, features_conf,
    effort_conf, dist, thresh=0.6835, tmp=tmp)

print('Gala:')
print(stats_gala)
