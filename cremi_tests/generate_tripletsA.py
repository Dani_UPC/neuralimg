# !/usr/bin/python

from neuralimg.image import datasets as dd

data_conf = '/DataDisk/morad/cremi/datasets/data.conf'
crag_path = '/DataDisk/morad/cremi/crags/sampleA/mc_heuristic_small6696/hdf/training_dataset.h5'
data_path = '/DataDisk/morad/cremi/datasets/dataA_small.h5'

print('Testing triplet dataset ...')
dgen_pair = dd.TripletDataGen(data_conf, data_path)
cragA = dd.DataGenOptions(crag_path, init=0, end=100, exclude=[])
dgen_pair.generate_dataset([cragA])
