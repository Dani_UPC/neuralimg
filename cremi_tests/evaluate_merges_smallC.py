#!/usr/bin/python

from neuralimg.evaluation import speval as ev
import os

# Important: for a fair evaluation, segments from neurons that are the same
# but not connected should be separated in an updated groundtruth version (what
# we call 'unconnected groundtruth')

superpixels = '/DataDisk/morad/cremi/superpixels/sampleC/mask11_sigma025_ted25'
truth = '/DataDisk/morad/cremi/groundtruth_large_unconnected/sampleC'
ted_shift = 25
split = False

# Sample C - Candidate MC
histories = os.path.join(superpixels, 'histories_mc_small20')
print('Evaluating candidate MC merge trees ...')
best, data = ev.search_threshold(superpixels, truth, histories, ted_shift,
    out_stats=os.path.join(superpixels, 'mc_merges_small20.dat'), split_bg=split)

print('Best configuration found:')
print(best)
print('All data')
print(data)
