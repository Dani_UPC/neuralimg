# !/usr/bin/python

from neuralimg.image import preproc as pr

# Cremi - Sample C
proc = pr.DatasetProc('/DataDisk/morad/cremi/membrane/sampleC')
proc.read()
proc.segment(11, 0.25)
proc.save_data('/DataDisk/morad/cremi/superpixels/sampleC/mask11_sigma025_ted25')
