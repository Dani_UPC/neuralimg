#!/usr/bin/python

from neuralimg.crag import merge_trees as mt
import os

if __name__ == '__main__':

    # Debug: CREMI
    gt_folder = '/DataDisk/morad/cremi/groundtruth_large_unconnected/sampleA'
    sps_folder = '/DataDisk/morad/cremi/superpixels/sampleA/mask3_sigma1_ted25'
    mems_folder = '/DataDisk/morad/cremi/membrane_norm/sampleA'

    # MC Extractor, test both formats
    mc_path = os.path.join(sps_folder, 'histories_mc_small15')
    mt.MCTreeExtractor(sps_folder, mems_folder, smallRegion=15).extract(mc_path)


