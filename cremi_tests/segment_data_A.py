# !/usr/bin/python

from neuralimg.image import preproc as pr

# Cremi - Sample A
proc = pr.DatasetProc('/DataDisk/morad/cremi/membrane/sampleA')
proc.read()
proc.segment(3, 1)
proc.save_data('/DataDisk/morad/cremi/superpixels/sampleA/mask3_sigma1')

