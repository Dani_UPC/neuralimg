#!/usr/bin/python

from neuralimg.evaluation import speval as ev
from neuralimg.crag import crag as cr
import os

if __name__ == '__main__':

    gt = '/DataDisk/morad/cremi/groundtruth_large/sampleA'
    sps_mc = '/DataDisk/morad/cremi/superpixels/sampleA/mask3_sigma1_ted25_thresh_mc0060392/'
    raws = '/DataDisk/morad/cremi/raw/sampleA'
    histories_mc = '/DataDisk/morad/cremi/superpixels/sampleA/mask3_sigma1_ted25/histories_mc'
    membranes = '/DataDisk/morad/cremi/membrane_norm/sampleA'
    out_mc = '/DataDisk/morad/cremi/crags/sampleA/mc_heuristic'

    create_conf = '/DataDisk/morad/cremi/crags/create_training_project.conf'
    features_conf = '/DataDisk/morad/cremi/crags/extract_training_features.conf'
    effort_conf = '/DataDisk/morad/cremi/crags/extract_best-effort.conf'

    # CRAG FOR MC SUPERPIXELS
    #print(ev.evaluate_crags(sps_mc, gt, raws, membranes, histories_mc, create_conf, features_conf,
    #    effort_conf, dists=[400, 800, 1200], nworkers=1, thresh=0.060392, 
    #    outp='crags_A.dat', tmp='/DataDisk/morad/temp/sampleA'))
    cragen = cr.CragGenerator(out_mc)
    cragen.generate_crag(gt, sps_mc, raws, membranes, create_conf,
        max_zlink=1200, histories=histories_mc, histories_thresh=0.060392)
    cragen.extract_features(features_conf)
    cragen.extract_best_effort(effort_conf, cr.LossType.ASSIGNMENT, os.path.join(out_mc, 'best'))

