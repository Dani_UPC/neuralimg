#!/usr/bin/python

from neuralimg.crag import crag as cr
import os

if __name__ == '__main__':

    gt = '/DataDisk/morad/cremi/groundtruth_large/singleA'
    sps_mc = '/DataDisk/morad/cremi/superpixels/singleA/mask3_sigma1_ted25/'
    raws = '/DataDisk/morad/cremi/raw/sampleA'
    histories_mc = os.path.join(sps_mc, 'histories_mc')
    membranes = '/DataDisk/morad/cremi/membrane_norm/singleA'
    out_mc = '/DataDisk/morad/cremi/crags/singleA/mc_heuristic'

    create_conf = '/DataDisk/morad/cremi/crags/create_training_project.conf'
    features_conf = '/DataDisk/morad/cremi/crags/extract_training_features.conf'
    effort_conf = '/DataDisk/morad/cremi/crags/extract_best-effort.conf'

    # CRAG FOR MC SUPERPIXELS
    cragen = cr.CragGenerator(out_mc)
    cragen.generate_crag(gt, sps_mc, raws, membranes, create_conf,
        max_zlink=500, histories=histories_mc)
    #cragen.extract_features(features_conf)
    #cragen.extract_best_effort(effort_conf, cr.LossType.HEURISTIC, os.path.join(out_mc, 'best'))

