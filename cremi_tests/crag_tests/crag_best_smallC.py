#!/usr/bin/python

from neuralimg.crag import crag as cr
import os

if __name__ == '__main__':

    out_mc = '/DataDisk/morad/cremi/crags/sampleC/mc_heuristic_small732'
    crag_path = os.path.join(os.path.join(out_mc, 'hdf'), 'training_dataset.h5')
    create_conf = '/DataDisk/morad/cremi/crags/create_training_project.conf'
    features_conf = '/DataDisk/morad/cremi/crags/extract_training_features.conf'
    effort_conf = '/DataDisk/morad/cremi/crags/extract_best-effort.conf'

    # CRAG FOR MC SUPERPIXELS
    cragen = cr.CragGenerator(out_mc)
    cragen.read_crag(crag_path)
    cragen.extract_best_effort(effort_conf, cr.LossType.HEURISTIC, 
        os.path.join(out_mc, 'best'), overwrite=True)

