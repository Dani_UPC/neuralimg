#!/usr/bin/python

from neuralimg.crag import crag as cr
import os

if __name__ == '__main__':

    out_mc = '/DataDisk/morad/cremi/crags/sampleA/mc_heuristic'
    crag_gen = cr.CragGenerator(out_mc)
    crag_gen.read_crag(os.path.join(out_mc, os.path.join('hdf', 'training_dataset.h5')))
    crag_gen.visualize(costs_name='best-effort_loss')

