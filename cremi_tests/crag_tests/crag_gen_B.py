#!/usr/bin/python

from neuralimg.crag import crag as cr
import os

if __name__ == '__main__':

    gt = '/DataDisk/morad/cremi/groundtruth_large/sampleB'
    sps_mc = '/DataDisk/morad/cremi/superpixels/sampleB/mask11_sigma05_ted25_mc00938/'
    raws = '/DataDisk/morad/cremi/raw/sampleB'
    histories_mc = '/DataDisk/morad/cremi/superpixels/sampleB/mask11_sigma05_ted25/histories_mc'
    membranes = '/DataDisk/morad/cremi/membrane_norm/sampleB'
    out_mc = '/DataDisk/morad/cremi/crags/sampleB/mc_heuristic'

    create_conf = '/DataDisk/morad/cremi/crags/create_training_project.conf'
    features_conf = '/DataDisk/morad/cremi/crags/extract_training_features.conf'
    effort_conf = '/DataDisk/morad/cremi/crags/extract_best-effort.conf'

    # CRAG FOR MC SUPERPIXELS
    cragen = cr.CragGenerator(out_mc)
    cragen.generate_crag(gt, sps_mc, raws, membranes, create_conf,
        max_zlink=350, histories=histories_mc, histories_thresh=0.0938)
    cragen.extract_features(features_conf)
    cragen.extract_best_effort(effort_conf, cr.LossType.ASSIGNMENT, os.path.join(out_mc, 'best'))

