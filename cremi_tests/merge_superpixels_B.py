#!/usr/bin/python

from neuralimg.crag import merge_trees as mt
import os

if __name__ == '__main__':

    root = '/DataDisk/morad/cremi/superpixels/sampleB'
    sps = os.path.join(root, 'mask11_sigma05_ted25')

    # Gala superpixels
    gala_thresh = 0.52
    histories_gala = os.path.join(sps, 'histories_gala')
    out_gala = os.path.join(root, 'mask11_sigma05_ted25_gala' + str(gala_thresh).replace('.', ''))
    mt.merge_dataset(sps, histories_gala, gala_thresh, out_gala)

    # MC superpixels
    mc_thresh = 0.0938
    histories_mc = os.path.join(sps, 'histories_mc')
    out_mc = os.path.join(root, 'mask11_sigma05_ted25_mc' + str(mc_thresh).replace('.', ''))
    mt.merge_dataset(sps, histories_mc, mc_thresh, out_mc)

